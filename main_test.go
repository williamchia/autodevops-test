package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestEndpoint(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Handler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("got status %v wanted %v", status, http.StatusOK)
	}

	expected := "Hello, World!\n"
	if body := rr.Body.String(); body != expected {
		t.Errorf("got body %v wanted %v", body, expected)
	}
}
