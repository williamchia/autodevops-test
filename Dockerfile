FROM golang:latest AS builder

WORKDIR /app

ADD go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 go build -o web

FROM scratch

WORKDIR /app

COPY --from=builder /app/web ./

EXPOSE 5000

ENTRYPOINT ["/app/web"]
